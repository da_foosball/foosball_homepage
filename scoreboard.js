var Refresh = {
  frequency: 1000,
  statusfreq: 30000,
  statuslast: 0,
  timer: null,
  state: "",
  tableStatus: "",
  secsSinceStatus: 0
};

var Status = false;

$(document).ready(setup);

function setup() {
  setFrequency(Refresh.frequency);
  update();
  //Timer=setInterval(getStatus, 2000);
}

function setFrequency(millis) {
  // If update frequency has changed
  if (!Refresh.Timer || millis != Refresh.frequency) {
    // Clear timer if it exists
    if (Refresh.timer) clearInterval(Refresh.timer);
    if (millis) Refresh.timer=setInterval(update, millis);
    // New refresh frequency (0 = off)
    Refresh.frequency=millis;
    // New state: auto, manual, off
    // Refresh.state=newstate;
  }
}

// Run frequency page updates
//   - Each time we chech time since last table status
//   - If larger than statusfreq, then get new status
//   - Otherwise just update page counters
//   - Also check rules for changing update and/or status frequencies
function update() {
  // How long time since last status
  Refresh.secsSinceStatus=Math.round((Date.now()-Refresh.statuslast)/1000);
  // If enough time passed since last table status, get new status
  if ((Date.now()-Refresh.statuslast) > Refresh.statusfreq) {
    getStatus();
    // Just return. update will be called again after new status is recieved
    return;
  }
  setUpdateFrequency();
  setVacant();
  setScore();
  setHeartbeat();
}

// Depending on table status, determiner
function setUpdateFrequency() {
  // Status was not recieved at all
  if (!Status.ok) {
    if (Refresh.secsSinceStatus > 3600) // No status recieved for more than an hour, and we can't get a status
      Refresh.statusfreq=1800000; // Check once every half hour
    else
      Refresh.statusfreq=900000; // Check once every 15 minutes
    return;
  }
  // Status recieved so table is online
  // ... but heartbeat is too old, so foosball_main is probably not running
  if (Status.heartbeat>1800)
    Refresh.statusfreq=600000; // Heartbeat more than 30 minutes old. Check once every 10 minutes
  else if (Status.heartbeat>120)
    Refresh.statusfreq=120000; // Heartbeat between 2 and 30 minutes old. Check once every 2 minutes
  // OK. Status recieved and heartbeat is less than 2 minutes old
  // This is default status when everything is running as expected
  else {
    // When table is occupied, make frequent updates (updates with every iteration)
    if (Status.vacant==0) {
      Refresh.statusfreq=1700;
      if (Refresh.frequency>1000) setFrequency(1000);
      return;
    }
    // OK. Table is vacant. Determine pdate frequency after how long time it has been vacant
    if (Status.vacant_time<300)
      Refresh.statusfreq=4000; // Check once every 4 seconds
    else if (Status.vacant_time<1800)
      Refresh.statusfreq=10000;
    else
      Refresh.statusfreq=30000; // Check once every 20 seconds
    return;
  }
  // Set local javascript client update frequency (counters)
}

// Fetch current table status and then call result function
function getStatus() {
  Refresh.state="updating";
  $.getJSON("status.php", recieveStatus);
}

function recieveStatus(data, updatestatus) {
  Refresh.state="done";
  Refresh.statuslast=Date.now();
  if (updatestatus=="success")
    Status=data;
  else
    Status={ok: 0};
  update();
}

function ps(num) {
  if (num==1) return("s");
  else return("");
}
function d(secs) {
  if (secs<86400) return("");
  return(Math.floor(secs/86400)+"d");
}
function h(secs) {
  if (secs<3600) return("");
  return(Math.floor((secs%86400)/3600)+"h");
}
function m(secs) {
  if (secs<60) return("");
  return(Math.floor((secs%3600)/60)+"m");
}
function s(secs) {
  if (secs>3600) return("");
  return(("0"+(secs%60)+"s").slice(-3));
}
function days(secs) {
  if (secs<86400) return("");
  var val=Math.floor(secs/86400);
  return(val+" day"+ps(val));
}
function minutes(secs) {
  if (secs<60) return("");
  var val=Math.floor((secs%3600)/60);
  return(val+" minute"+ps(val));
}

function sec2string(secs) {
  return(d(secs)+h(secs)+m(secs)+s(secs));
}

function showRefresh() {
  if (Refresh.statusfreq==0) return("<br> Opdatering stoppet");
  else if (Refresh.statusfreq>120000) 
    return("<br> Opdateringsfrekvens: "+parseInt(Refresh.statusfreq/60000)+" minutter");
  else
    return("<br> Opdateringsfrekvens: "+Math.ceil(Math.max(Refresh.statusfreq,Refresh.frequency)/1000)+" sekunder");
}

function setHeartbeat(heartbeat) {
  if (Refresh.state=="updating")
    $("#heartbeat").html("Updating..."+showRefresh());
  else
    // Time since message from control program: statustime+heartbeat at that time
    $("#heartbeat").html(sec2string(Refresh.secsSinceStatus)+" siden sidste besked fra bordet"+showRefresh());
}

function setVacant() {
  if (Status.ok) {
    if (Status.heartbeat > 130) {
      if (Refresh.tableStatus!="nocontrol") {
        $("#vacant_img").attr("src","/scoreboard/images/ball_blue.png");
        $("#vacant_txt").html("Bordet er online, men overvågning kører ikke");
        Refresh.tableStatus="nocontrol";
      }
    }
    else if (Status.vacant) {
      if (Refresh.tableStatus!="vacant") {
        $("#vacant_img").attr("src","/scoreboard/images/ball_green.png");
        Refresh.tableStatus="vacant";
      }
      $("#vacant_txt").html("Bordet har været ledigt i "+sec2string(Status.vacant_time+Refresh.secsSinceStatus));
    }
    else {
      if (Refresh.tableStatus!="occupied") {
        $("#vacant_img").attr("src","/scoreboard/images/ball_red.png");
        Refresh.tableStatus="occupied";
      }
      $("#vacant_txt").html("Bordet har været optaget i "+sec2string(Status.vacant_time+Refresh.secsSinceStatus));
    }
  }
  else if (Refresh.tableStatus!="nocontact") {
    $("#vacant_img").attr("src","/scoreboard/images/ball_blue.png");
    $("#vacant_txt").html("Bordet kan ikke kontaktes PT");
    Refresh.tableStatus="nocontact";
  }
}

function setGoals(goal1,goal2,blinkOnChange) {
  if (blinkOnChange) {
    var g1=$("#goal1").html();
    var g2=$("#goal2").html();
    if (g1.trim()==="" || g1!=goal1)
      $("#goal1").fadeOut('slow', function() {$(this).html(goal1+" ").fadeIn('slow').fadeOut('slow').fadeIn('slow');});
    if (g2.trim()==="" || g2!=goal2)
      $("#goal2").fadeOut('slow', function() {$(this).html(" "+goal2).fadeIn('slow').fadeOut('slow').fadeIn('slow');});
  }
  else {
    $("#goal1").html(goal1+" ");
    $("#goal2").html(" "+goal2);
  }
}

function setScore() {
  // console.log("Heartbeat: "+Status.heartbeat+", OK: "+Status.ok+", Vacant: "+Status.vacant);
  // Seneste besked fra bordet blev modtaget korrekt
  if (Status.ok) {
    // Under 3 minutter siden seneste heartbeat - alt kører som det skal
    if (Status.heartbeat<120) {
      if (Status.vacant==1) {
        if (Status.team1>0 || Status.team2>0) {
          $("#scoreheader").html("Seneste resultat");
          setGoals(Status.team1,Status.team2,false);
        }
        else {
          $("#scoreheader").html("Ingen aktuel score");
          setGoals("","",false);
        }
        $("#scorelastgoal").html("-");
      }
      else {
        $("#scoreheader").html("Aktuel stilling");
        if (Status.team1==0 && Status.team2==0)
          $("#scorelastgoal").html(sec2string(Status.score_time+Refresh.secsSinceStatus)+" siden nulstilling");
        else
          $("#scorelastgoal").html(sec2string(Status.score_time+Refresh.secsSinceStatus)+" siden sidste mål");
        setGoals(Status.team1,Status.team2,true);
      }
    }
    else {
      // More than 3 minutes since heartbeat. foosball_main probably not running.
      $("#scoreheader").html("");
      $("#scorelastgoal").html("-");
      $("#goal1").html("");
      $("#goal2").html("");
    }
  }
  else {
    // No response when trying to get status. Net connection probably dead
    $("#scoreheader").html("Ingen stilling modtaget.");
    $("#scorelastgoal").html("-");
    $("#goal1").html("");
    $("#goal2").html("");
  }
}


function nulstil() {
  $.getJSON("reset_score.php", getStatus);
}
