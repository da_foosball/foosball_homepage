#include <signal.h>
#include <stdio.h>

int readPid() {
  char *file="foosball_main.pid";
  FILE *f;
  int pid;
  
  f=fopen(file,"r");
  if (f) {
    fscanf(f,"%d",&pid);
    fclose(f);
  }
  return(pid);
  
}

int main(int argc, char **argv) {
  int pid=readPid();
  int killerror;

  if (pid<=0) {
    printf("Can't read PID from file\n");
    return(100);
  }
  else
    killerror=kill(pid,SIGUSR1);

  if (killerror)
    printf("Can't send SIGUSR1 to pid %d\n",pid);
  return(killerror);
}

