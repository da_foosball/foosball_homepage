<?php

require("docheader.inc");
require("getvar.inc");
ini_set("display_errors",1);

$header=new DocHeader();
$header->set_title("Set bordfodbold score");
$get=new Getvars();

$pass=$get->add("pass", null, -1);
if ($pass==-1) {
  $pass="";
  $message="";
}
else if ($pass!="gogoset") 
  $message="Unauthorized";
else {
  $goal1=$get->add("goal1", null, -1);
  $goal2=$get->add("goal2", null, -1);
  $message="";

  $m1=preg_match("/^\d\d?$/",$goal1);
  $m2=preg_match("/^\d\d?$/",$goal2);

  if ($goal1!=-1 || $goal2!=-1) {
    if ($m1==0 || $m2==0) {
      $message="Fejl i indtastede score. Kun tal mellem 0 og 99 tilladt: '$goal1','$goal2'";
    }
    else {
      file_put_contents("correctscore.txt", "$goal1\n$goal2\n");
      $message="Scoren sat til $goal1 - $goal2";
      # Try to signal foosball_main python program
      $returnval=0;
      $output="";
      $res=exec('./signal_foosball', $output, $returnval);
      if ($returnval)
        $message.="<br>Signal failed: ".implode(" ",$output);
      else
        $message.="<br>Signal send successfully";
    }
  }
}


$header->display();
print("<body>\n");
print("$message<p>\n");
?>
<form action="#" method="GET">
  Authorize: <input type="password" name="pass" size="4" value="<?php print("$pass");?>"><br>
  Score: <input name="goal1" size="2"> - <input name="goal2" size="2"> <input type="submit" value="Sæt score">
</form>
</body>
</html>

