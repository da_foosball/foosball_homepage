<?php
  
  $heartfile="heartbeat";
  $vacantfile="vacant.txt";
  $scorefile="score.txt";

  function error($msg, $vars=array()) {
    print(json_encode(array_merge(array("ok"=>0,"error"=>$msg),$vars))."\n");
    die();
  }

  $now=time();
  $info=array();
  $info["server_time"]=$now;

  // Read time since last table heartbeat
  $heart_mtime   = @filemtime($heartfile)
    or error("Can't read heartbeat file", $info);
  $heart_time=$now-$heart_mtime;
  $info["heartbeat"]=$heart_time;

  // Read time since last status change
  $vacant_mtime   = @filemtime($vacantfile)
    or error("Can't read vacant status file", $info);
  $vacant_time=$now-$vacant_mtime;
  $info["vacant_time"]=$vacant_time;

  // Read vacant status
  $vacant_content = @file_get_contents($vacantfile);
  $vacant=(int)$vacant_content;
  if ($vacant!==0 &&  $vacant!==1)
    error("Error parsing vacant file", $info);
  $info["vacant"]=$vacant;

  // Read time since last goal update
  $score_mtime   = @filemtime($scorefile)
    or error("Can't read score file", $info);
  $score_time=$now-$score_mtime;
  $info["score_time"]=$score_time;

  // Read current score
  $score_content = @file_get_contents($scorefile);
  if (!preg_match("/[0-9][0-9]* - [0-9][0-9]*/",$score_content))
    error("Error parsing score",$info);
  $goal1=(int)trim(preg_replace("/ -.*/","",$score_content));
  $goal2=(int)trim(preg_replace("/.*- /","",$score_content));
  if ($goal1>99 || $goal2>99)
    error("Error: Too many goals", $info);
  $info["team1"] = $goal1;
  $info["team2"] = $goal2;

  // Everything read and parsed. Return
  $info["ok"]=1;
  print(json_encode($info)."\n");
?>
