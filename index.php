<?php

require("docheader.inc");

$header=new DocHeader();
$header->set_html5();
$header->add_javascript("scoreboard.js");
$header->add_jquery();
$header->add_css("scoreboard.css");
$header->set_title("SKAT Bordfodbold");
$header->set_icon("/favicon.ico");

$header->display();

print("<body>\n".
      "  <div id='header'>\n".
      "    SKAT Tornado-bord\n".
      "  </div>\n".
      "  <div id='vacant'>\n".
      "    <img id='vacant_img' alt='Table status icon' src='images/ball_blue.png'><br>\n".
      "    <span id='vacant_txt'>Ukendt status</span>\n".
      "  </div>\n".
      "  <div>\n".
      "    <span id='scoreheader'>Ukendt stilling</span><br>\n".
			"    <span id='scorecontent'><span id='goal1'></span>-<span id='goal2'></span></span><br>\n".
      "    <span id='scorelastgoal'>(Ingen kontakt til fodboldbordet endnu)</span>\n".
			"  </div>\n".
      "  <div id='divreset'>\n".
      "    <input id='reset' type='submit' value='' onclick='nulstil();return(false);'/>\n".
      "  </div>\n".
      "  <div id='heartbeat'>\n".
      "  </div>\n".
			"</body>\n".
			"</html>\n");

?>
