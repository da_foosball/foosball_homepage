Foosball Homepage
==================

Website hosted on the Raspberry Pi controlling the Foosball table.

Currently only the scoreboard part is included, and located in the root
directory and NOT in the scoreboard subdirectory as is the plan.

The website will EVENTUALLY include:
  1. Scoreboard
    * Shows the occupied/unoccupied status and current score
  2. Web setup [NOT implemented yet]
    * Module where players can configure setup options for the table
    * This can be controlled from the menu on the table as well, but it is typically easier on the website
  3. Ranking model [NOT implmented yet]
    * We use a trueskill ranking model, and this can be viewed and controlled here
  4. Administration [NOT implemented yet]
    * Administrator interface. Check health and setup system / software


Scoreboard
----------

The scoreboard is located in the scoreboard subdirectory.
